# Hello Shadow-cljs

A sample project demonstrating how to use clojurescript with
shadow-cljs.


## Installing

- Install `npm` using OS package manager.

- Install `shadow-cljs` command line tool with `npm install -g shadow-cljs`.

- Install npm dependencies from `package.json` using `npm install`.


## Building and running

Start watched compilation with `shadow-cljs watch app`. After building, an
HTTP server will run at `http://localhost:8000/index.html`.


## Extra note

- Take a look at these following files:

    - shadow-cljs.edn : The build specification file.
    - package.json : NPM dependency file.
    - src/hello_shadow/core.cljs : Clojurescript source.
    
- To install npm dependencies use `--save-dev` flag to save
  the new library to `package.json` file. i.e.:
  
     npm install --save-dev react
     
- Translating javascript "require directives" to clojurescript
  might get a little frustrating if you don't know about so called
  "default exports"(who the fuck came up with that idea anyway?!!).
  Take a look at "Table 1." under "12.1.1. Using npm packages" section
  in shadow-cljs user guide for clarence: https://shadow-cljs.github.io/docs/UsersGuide.html#npm
  
- Note that `node_modules`,`.shadow_cljs` and `/public/js/` directories are excluded
  from source control using `.gitignore`. As they are build artifacts, they should
  not be included in source control.
  
  
## Author

Titon Barua <titon@vimmaniac.com>
