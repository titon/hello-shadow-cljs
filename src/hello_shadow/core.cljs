(ns hello-shadow.core
  (:require [reagent.core :as r]
            [goog.string :as gstr]
            ["@material-ui/core/Button" :default Button]))

(enable-console-print!)

(def click-count (r/atom 0))

(defn app
  []
  [:div
   [:div {:style {:color         "#33f"
                  :margin-bottom "1rem"}}
    (gstr/format "You have clicked %s times!" @click-count)]

   [:div
    [:> Button
     {:variant  "contained"
      :color    "primary"
      :on-click #(swap! click-count inc)}
     "Click me!"]]])


(defn mount-app
  []
  (r/render [app]
            (.getElementById js/document "app")))

(mount-app)
